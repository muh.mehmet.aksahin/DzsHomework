package com.dzsi.homework.util.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class PeekingIterator<E> implements Iterator<E> {

	private final Iterator<E> iterator;
	private E nextElement;

	public PeekingIterator(Iterator<E> iterator) {
		this.iterator = iterator;
		if (iterator.hasNext()) {
			nextElement = iterator.next();
		}
	}

	public E peek() {
		if (nextElement == null) {
			throw new NoSuchElementException("No more elements");
		}
		return nextElement;
	}

	public boolean hasNext() {
		return nextElement != null;
	}

	@Override
	public void remove() {
		iterator.remove();
	}

	public E next() {
		E res = nextElement;
		if (iterator.hasNext()) {
			nextElement = iterator.next();
		} else {
			nextElement = null;
		}
		return res;
	}


}
