package com.dzsi.homework.util;

import com.dzsi.homework.util.util.PeekingIterator;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * [1,2,3] <br>
 * PeekingIterator pi = ...; <br>
 * pi.next() = 1, [2,3] <br>
 * pi.peek() = 2  [2,3] <br>
 * pi.next() = 2  [3] <br>
 * pi.peek() = 3  [3] <br>
 * pi.peek() = 3 [3] <br>
 * pi.next() = 3 [3] <br>
 * pi.peek() = error <br>
 * <br>
 * [1] <br>
 * pi.peek() = 1; <br>
 * pi.hasNext() = true; <br>
 * pi.next() = 1; <br>
 * pi.peek() = error <br>
 */
class PeekIteratorTest {

	@Test
	void first_Accepted_Criteria_for_Homework() {
		// given
		List<Integer> list = Arrays.asList(1, 2, 3);
		PeekingIterator<Integer> pi = new PeekingIterator<>(list.iterator());

		//when
		final String expectedMessage = "No more elements";
		final int expectedElementFirst = 1;
		final int expectedElementSecond = 2;
		final int expectedElementThird = 3;

		// then
		assertEquals(expectedElementFirst, (int) pi.next());
		assertEquals(expectedElementSecond, (int) pi.peek());
		assertEquals(expectedElementSecond, (int) pi.next());
		assertEquals(expectedElementThird, (int) pi.peek());
		assertEquals(expectedElementThird, (int) pi.peek());
		assertEquals(expectedElementThird, (int) pi.next());

		Exception exception = assertThrows(NoSuchElementException.class, pi::peek);
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));

	}

	@Test
	void second_Accepted_Criteria_for_Homework() {
		// given
		List<Integer> list = List.of(1);
		PeekingIterator<Integer> pi = new PeekingIterator<>(list.iterator());

		//when
		final String expectedMessage = "No more elements";
		final int expectedElementFirst = 1;
		final boolean expectedHasNext = true;

		// then
		assertEquals(expectedElementFirst, (int) pi.peek());
		assertEquals(expectedHasNext, pi.hasNext());
		assertEquals(expectedElementFirst, (int) pi.next());

		Exception exception = assertThrows(NoSuchElementException.class, pi::peek);
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));

	}

}
